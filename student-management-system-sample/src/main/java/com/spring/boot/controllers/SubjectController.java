package com.spring.boot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.spring.boot.converters.SubjectConverter;
import com.spring.boot.dto.SubjectDto;
import com.spring.boot.services.SubjectService;

@RestController
@RequestMapping(value = "api/v1") // Thread path URL
public class SubjectController {

	@Autowired
	private SubjectService subjectService;

	@PostMapping(value = "/subject") // Last path URL // Working Success!!!
	public ResponseEntity<Object> addSubject(@RequestBody SubjectDto subjectDto) {
		subjectService.addSubject(SubjectConverter.subjectDtoToSubject(subjectDto));
		return new ResponseEntity<Object>("Added Successfully", HttpStatus.CREATED);
	}

	@PutMapping(value = "/subject/{id}") // Working Success!!!
	public ResponseEntity<Object> updateSubject(@RequestBody SubjectDto subjectDto, @PathVariable Long id ) {
		if (subjectService.subTableId(id)) {
			subjectService.updateSubjectById(SubjectConverter.subjectDtoToSubject(subjectDto));
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Invalide subject id!!!", HttpStatus.OK);
		}
		
	}

	@GetMapping(value = "/subject/{id}") // Working Success!!!
	public ResponseEntity<Object> getSubject(@PathVariable Long id) {
		if (subjectService.subTableId(id)) {
			return new ResponseEntity<Object> (subjectService.getSubjectid(id), HttpStatus.OK);
		}
		return new ResponseEntity<Object>("Invalide subject id!!!",HttpStatus.OK);
	}

	@DeleteMapping(value = "subject/{id}") // Working Success!!!
	public void deleteSubject(@PathVariable Long id) {
		subjectService.deleteById(id);
	}

}