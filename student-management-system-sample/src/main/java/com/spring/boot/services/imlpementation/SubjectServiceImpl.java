package com.spring.boot.services.imlpementation;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Subject;
import com.spring.boot.repositories.SubjectRepository;
import com.spring.boot.services.SubjectService;

@Service
public class SubjectServiceImpl implements SubjectService {
	@Autowired
	private SubjectRepository subjectRepository;

	@Override
	public void addSubject(Subject subject) {
		subjectRepository.save(subject);
	}

	@Override
	public Subject getSubjectid(Long id) {
		subjectRepository.findById(id).get();
		return null;
	}

	@Override
	public void updateSubjectById(Subject subject) {
		subjectRepository.save(subject);
	}

	@Override
	public void deleteById(Long id) {
		subjectRepository.deleteById(id);
	}
	
	@Override
	public boolean subTableId(Long id) {
		return false;
	}

}