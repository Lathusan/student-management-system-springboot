package com.spring.boot.services;

import java.util.Optional;

import com.example.demo.entities.Subject;

public interface SubjectService {

	public void addSubject(Subject subject);
	
	public Subject getSubjectid(Long id);

	public void updateSubjectById(Subject subject);

	public void deleteById(Long id);
	
	public boolean subTableId(Long id);

}