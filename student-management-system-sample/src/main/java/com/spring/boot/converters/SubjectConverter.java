package com.spring.boot.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.entities.Subject;
import com.spring.boot.dto.SubjectDto;

@Service
public class SubjectConverter {

	// projectDto to Project Entity Converter

	public static Subject subjectDtoToSubject(SubjectDto subjectDto) {
		Subject subject = new Subject();
		if (subjectDto != null) {
			subject.setId(subjectDto.getId());
			subject.setSubject(subjectDto.getSubject());
			subject.setDescription(subjectDto.getDescription());			
			return subject;
		}
		return null;
	}

	// Project to ProjectDto list converter

	public static List<SubjectDto> subjectToSubjectDto(List<Subject> subjectList) {
		List<SubjectDto> listSubjectDto = new ArrayList<>();
		if (subjectList != null) {
			for (Subject subject : subjectList) {
				SubjectDto subjectDto = new SubjectDto();
				subjectDto.setId(subject.getId());
				subjectDto.setSubject(subject.getSubject());
				subjectDto.setDescription(subject.getDescription());

				listSubjectDto.add(subjectDto);
			}
			return listSubjectDto;
		}
		return null;
	}

}